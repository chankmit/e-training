<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class MainController extends Controller
{

    public function index(request $request){ 
        $showpage=''; 
        return view('demo.index',['showpage'=>$showpage]); 
    }
    
    public function orders(){ 
        $showpage='orders';
        
        $client = new \GuzzleHttp\Client(); 
        $response = $client->post(env('API_URL').'enrollUserGet', [
        'json' => [
            'language' => env('API_LANGUAGE'),
            'userAgent' => env('API_USERAGENT'),
            'actionBy' => env('API_ACTIONBY'),
            'sessionRefCode' => env('API_SESSIONREFCODE'),
            'sessionId' => env('API_SESSIONID'),
            'selectSet' => env('API_SELECTSET'), 
        ]]); 
        $response = json_decode($response->getBody(), true);
        //return $response;
        $trainingSession= $response['trainingSession'];  
        return view('demo.orders',[
            'showpage'=>$showpage,
            'trainingSession'=>$trainingSession, 
        ]);
    }
    
    public function news(){
        
        $showpage='news';
        
        $client = new \GuzzleHttp\Client(); 
        $response = $client->post(env('API_URL').'newsListGet', [
        'json' => [
            'language' => env('API_LANGUAGE'),
            'userAgent' => env('API_USERAGENT'),
            'actionBy' => env('API_ACTIONBY'),
            'sessionRefCode' => env('API_SESSIONREFCODE'),
            'sessionId' => env('API_SESSIONID'),
            'selectSet' => env('API_SELECTSET'),
            'selectLimit' => env('API_SELECTLIMIT'),
        ]]); 
        $response = json_decode($response->getBody(), true);
        //return $response;
        $newsTopList= $response['newsTopList']; 
        $newsList= $response['newsList'];
        
        return view('demo.news',['showpage'=>$showpage, 'newsTopList'=>$newsTopList, 'newsList'=>$newsList]);

    }
    public function courses(){
        $client = new \GuzzleHttp\Client(); 
        $response = $client->post(env('API_URL').'enrollCourseTypeGet', [
        'json' => [
            'language' => env('API_LANGUAGE'),
            'userAgent' => env('API_USERAGENT'),
            'actionBy' => env('API_ACTIONBY'),
            'sessionRefCode' => env('API_SESSIONREFCODE'),
            'sessionId' => env('API_SESSIONID'),
            'selectSet' => env('API_SELECTSET'),
            'selectLimit' => env('API_SELECTLIMIT'),
        ]]); 
        $response = json_decode($response->getBody(), true);
        //return $response;
        $courseTypeTopList= $response['courseTypeTopList'];
        $courseTypeList= $response['courseTypeList'];
        
        $showpage='courses';
        return view('demo.courses',['showpage'=>$showpage, 'courseTypeTopList'=>$courseTypeTopList, 'courseTypeList'=>$courseTypeList]);
    }
    
    public function subjects(request $request){
        $courseCode=$request->courseCode;
        $courseName=$request->courseName;
        $showpage='courses';
        
        $client = new \GuzzleHttp\Client(); 
        $response = $client->post(env('API_URL').'enrollCourseListGet', [
        'json' => [
            'language' => env('API_LANGUAGE'),
            'userAgent' => env('API_USERAGENT'),
            'actionBy' => env('API_ACTIONBY'),
            'sessionRefCode' => env('API_SESSIONREFCODE'),
            'sessionId' => env('API_SESSIONID'),
            'selectSet' => env('API_SELECTSET'),
            'selectLimit' => env('API_SELECTLIMIT'), 
            'regionCode' => "",
            'courseTypeCode' => $courseCode ,
        ]]); 
        $response = json_decode($response->getBody(), true);
        //return $response;
        $courseTopList= $response['courseTopList'];
        $courseList= $response['courseList'];
        
        return view('demo.subjects',[
            'showpage'=>$showpage, 
            'courseCode'=>$courseCode,
            'courseName'=>$courseName,
            'courseTopList'=>$courseTopList,
            'courseList'=>$courseList
        ]);
    }
    
    public function calendars(request $request){ 
        $courseCode=$request->courseCode;
        $courseName=$request->courseName;
        $subCode=$request->subCode;
        $subName=$request->subName;
        
        $client = new \GuzzleHttp\Client(); 
        $response = $client->post(env('API_URL').'enrollSessionCalendarGet', [
        'json' => [
            'language' => env('API_LANGUAGE'),
            'userAgent' => env('API_USERAGENT'),
            'actionBy' => env('API_ACTIONBY'),
            'sessionRefCode' => env('API_SESSIONREFCODE'),
            'sessionId' => env('API_SESSIONID'),
            'selectSet' => env('API_SELECTSET'),
            'selectLimit' => env('API_SELECTLIMIT'), 
            'monthNumber' => date('m'),
            'yearNumber' => date('Y'),
            'regionCode' => "",
            'courseCode' => $subCode
        ]]); 
        $response = json_decode($response->getBody(), true);
        //return $response;
        $trainingMonthlyList= $response['trainingMonthlyList']; 
        
        $showpage='courses';
        return view('demo.calendars',[
            'showpage'=>$showpage,
            'courseName'=>$courseName,
            'courseCode'=>$courseCode,
            'subCode'=>$subCode,
            'subName'=>$subName,
            'trainingMonthlyList'=>$trainingMonthlyList
        ]);
    }
    
    public function usersselect(request $request){
        $courseCode=$request->courseCode;
        $courseName=$request->courseName;
        $subCode=$request->subCode;
        $subName=$request->subName;
        $trainCode=$request->trainCode;
        $startDate=$request->startDate;
        $endDate=$request->endDate;
        $startTime=$request->startTime;
        $endTime=$request->endTime;
        
        $client = new \GuzzleHttp\Client(); 
        $response = $client->post(env('API_URL').'personCanEnrollGet', [
        'json' => [
            'language' => env('API_LANGUAGE'),
            'userAgent' => env('API_USERAGENT'),
            'actionBy' => env('API_ACTIONBY'),
            'sessionRefCode' => env('API_SESSIONREFCODE'),
            'sessionId' => env('API_SESSIONID'),
            'trainingCode' => $trainCode,
            'dealerCode' => '110017'
        ]]); 
        $response = json_decode($response->getBody(), true);
        $personCanEndroll=$response['personList'];
        //return $personCanEndroll;
        $showpage='courses';
        return view('demo.userselect',[
            'showpage'=>$showpage, 
            'courseName'=>$courseName,
            'courseCode'=>$courseCode,
            'subCode'=>$subCode,
            'subName'=>$subName,
            'trainCode'=>$trainCode,
            'startDate'=>$startDate,
            'endDate'=>$endDate,
            'startTime'=>$startTime,
            'endTime'=>$endTime,
            'personCanEnroll'=>$personCanEndroll
        ]);
    }
    
    public function services(request $request){  
        $courseCode=$request->courseCode;
        $courseName=$request->courseName;
        $subCode=$request->subCode;
        $subName=$request->subName;
        $trainCode=$request->trainCode;
        $startDate=$request->startDate;
        $endDate=$request->endDate;
        $startTime=$request->startTime;
        $endTime=$request->endTime;
        
        $userIDselect=$request->userIDselect;
        $userNameselect=$request->userNameselect;
        $userBranchselect=$request->userBranchselect;
        $userImgselect=$request->userImgselect;
        
        /////////
        $userID=explode(",",$userIDselect);
        $userName=explode(",",$userNameselect);
        $userBranch=explode(",",$userBranchselect);
        $userImg=explode(",",$userImgselect);
        if(count($userID)>0){
            for($i=0;$i<count($userID);$i++){
                
                $userSelect[$i]['userCode']=$userID[$i];
                $userSelect[$i]['fullName']=$userName[$i];
                $userSelect[$i]['branchName']=$userBranch[$i];
                $userSelect[$i]['userImg']=$userImg[$i];
            }
        }else{
            $userSelect[0]['userCode']=$userIDselect;
            $userSelect[0]['fullName']=$userNameselect;
            $userSelect[0]['branchName']=$userBranchselect;
            $userSelect[0]['userImg']=$userImgselect;
        }
        //return $userSelect;
        /////////
        $client = new \GuzzleHttp\Client(); 
        $response = $client->post(env('API_URL').'enrollOptionListGet', [
        'json' => [
            'language' => env('API_LANGUAGE'),
            'userAgent' => env('API_USERAGENT'),
            'actionBy' => env('API_ACTIONBY'),
            'sessionRefCode' => env('API_SESSIONREFCODE'),
            'sessionId' => env('API_SESSIONID'),
            'trainingCode' => $trainCode
        ]]); 
        $response = json_decode($response->getBody(), true);
        $optionList=$response['optionList'];
        
        $showpage='courses';
        return view('demo.services',[
            'showpage'=>$showpage,
            'courseName'=>$courseName,
            'courseCode'=>$courseCode,
            'subCode'=>$subCode,
            'subName'=>$subName,
            'trainCode'=>$trainCode,
            'startDate'=>$startDate,
            'endDate'=>$endDate,
            'startTime'=>$startTime,
            'endTime'=>$endTime,
            'userIDselect'=>$userIDselect,
            'userNameselect'=>$userNameselect,
            'optionList'=>$optionList,
            'userBranchselect'=>$userBranchselect,
            'userSelect'=>$userSelect
        ]);
    }
    
    public function regis(request $request){
        $request->all();
        $showpage='courses';
        
        $courseCode=$request->courseCode;
        $courseName=$request->courseName;
        $subCode=$request->subCode;
        $subName=$request->subName;
        $trainCode=$request->trainCode;
        $startDate=$request->startDate;
        $endDate=$request->endDate;
        $startTime=$request->startTime;
        $endTime=$request->endTime;
        
        $userIDselect=$request->userIDselect;
        $userNameselect=$request->userNameselect;
        $userBranchselect=$request->userBranchselect;
        
        $userID=explode(",",$userIDselect);
        $userName=explode(",",$userNameselect);
        $userBranch=explode(",",$userBranchselect);
        
        $checkUserselect=$request->checkUser;
        $checkOptionselect=$request->checkOption;
        $checkDateselect=$request->checkDate;
        
        $checkUser=explode(",",$checkUserselect);
        $checkOption=explode(",",$checkOptionselect);
        $checkDate=explode(",",$checkDateselect);
        $personlist=[];
        //return $request;
        for($i=0;$i<count($checkUser); $i++){
            $personlist[$i]['userCode']=$checkUser[$i];
            $personlist[$i]['optionsList'][0]['optionCode']=$checkOption[$i];
            $personlist[$i]['optionsList'][0]['ansDetail']=$checkDate[$i];
        } 
        // $personlist;
        $client = new \GuzzleHttp\Client(); 
        $response = $client->post(env('API_URL').'enrollUserCreate', [
        'json' => [
            'language' => env('API_LANGUAGE'),
            'userAgent' => env('API_USERAGENT'),
            'actionBy' => env('API_ACTIONBY'),
            'sessionRefCode' => env('API_SESSIONREFCODE'),
            'sessionId' => env('API_SESSIONID'),
            'trainingCode' => $trainCode,
            'personList' => $personlist 
        ]]); 
        $response = json_decode($response->getBody(), true); 
        //return $response;
        //responseStatus
        return view('demo.regis',['showpage'=>$showpage]); 
    }
    
    public function logout(){
        $client = new \GuzzleHttp\Client(); 
        $response = $client->post(env('API_URL').'logOut', [
        'json' => [
            'language' => env('API_LANGUAGE'),
            'userAgent' => env('API_USERAGENT'),
            'actionBy' => env('API_ACTIONBY'),
            'sessionRefCode' => env('API_SESSIONREFCODE'),
            'sessionId' => env('API_SESSIONID'),
            'selectSet' => env('API_SELECTSET'),
            'selectLimit' => env('API_SELECTLIMIT'),
        ]]); 
        $response = json_decode($response->getBody(), true);
        //return $response; 
        Session::flush();
        /*
        session([
                    'userCode' => '',
                    'fullName' => '',
                    'imgProfile' => '',
                    'infoStatus' => '',
                    'userPositionCode' => '',
                    'userPositionName' => '',
                    'dealerCode' =>'',
                    'dealerName' => '',
                    'branchCode' => '',
                    'branchName' => '',
                    'menuList'=> '',
                ]); 
        */
        return redirect('/login');
    }
    
    
    public function login(request $request){
        $showpage='';
        //$request->session()->flash('flash_error', 'กรุณาตรวจสอบชื่อผู้ใช้หรือรหัสผ่าน ให้ครบถ้วนและถูกต้องด้วยค่ะ');
        if ($request->session()->has('userCode')) {
            return redirect('/');
        }else{
            return view('demo.login',['showpage'=>$showpage]);
        }
    }
    
    public function dologin(request $request){
        $username=$request->username;
        $password=$request->password;
        $password=md5($password);
        $showpage=''; 
        //return $password;
        if($username && $password){
            $client = new \GuzzleHttp\Client(); 
            $response = $client->post(env('API_URL').'logIn', [
            'json' => [
                'language' => env('API_LANGUAGE'),
                'userAgent' => env('API_USERAGENT'),
                'actionBy' => env('API_ACTIONBY'),
                'sessionRefCode' => env('API_SESSIONREFCODE'),
                'sessionId' => env('API_SESSIONID'),
                'username' => $username,
                'password' => $password
            ]]);  
            $response = json_decode($response->getBody(), true);
            //return $response;
            
            if($response['responseStatus']=='SUCCESS'){
                //return $response; 
                session([
                    'userCode' => $response['userCode'],
                    'fullName' => $response['fullName'],
                    'imgProfile' => $response['imgProfile'],
                    'infoStatus' => $response['infoStatus'],
                    'userPositionCode' => $response['userPositionCode'],
                    'userPositionName' => $response['userPositionName'],
                    'dealerCode' => $response['dealerCode'],
                    'dealerName' => $response['dealerName'],
                    'branchCode' => $response['branchCode'],
                    'branchName' => $response['branchName'],
                    'menuList'=> $response['menuList'],
                ]); 
                $request->session()->flash('flash_error', 'ยินดีต้อนรับคุณ '.$response['fullName']);
                //return view('demo.index',['showpage'=>$showpage]);
                return redirect('/');
            }else{ 
                $request->session()->flash('flash_error', 'กรุณาตรวจสอบชื่อผู้ใช้หรือรหัสผ่าน ให้ครบถ้วนและถูกต้องด้วยค่ะ');
                //return view('demo.login',['showpage'=>$showpage]);
                return redirect('/login');
            }
        }else{
            $request->session()->flash('flash_error', 'กรุณาตรวจสอบชื่อผู้ใช้หรือรหัสผ่าน ให้ครบถ้วนและถูกต้องด้วยค่ะ');
            //return view('demo.login',['showpage'=>$showpage]);
            return redirect('/login');
        } 
    }
    
    public function checkin(request $request){
        $showpage='manage'; 
        $trainingCode=$request->trainingCode;
        $startDate=$request->startDate;
        $endDate=$request->endDate;
        $startTime=$request->startTime;
        $endTime=$request->endTime;
        $selectDate=$request->selectDate;
        
        $client = new \GuzzleHttp\Client(); 
        $response = $client->post(env('API_URL').'enrollUserGet', [
        'json' => [
            'language' => env('API_LANGUAGE'),
            'userAgent' => env('API_USERAGENT'),
            'actionBy' => env('API_ACTIONBY'),
            'sessionRefCode' => env('API_SESSIONREFCODE'),
            'sessionId' => env('API_SESSIONID'),
            'selectSet' => env('API_SELECTSET'), 
        ]]); 
        $response = json_decode($response->getBody(), true);
        //return $response;
        $trainingSession= $response['trainingSession'];  
        
        return view('demo.checkin',[
            'showpage'=>$showpage,
            'trainingSession'=>$trainingSession, 
            'trainingCode'=>$trainingCode,
            'startDate'=>$startDate,
            'endDate'=>$endDate,
            'startTime'=>$startTime,
            'endTime'=>$endTime,
            'selectDate'=>$selectDate,
        ]); 
    }
    
    public function report(request $request){
        $showpage='manage'; 
        return view('demo.report',['showpage'=>$showpage]); 
    }
    public function manage(request $request){
        $showpage='manage'; 
        return view('demo.manage',['showpage'=>$showpage]); 
    }
}
