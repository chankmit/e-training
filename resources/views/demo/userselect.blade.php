@extends('template.main')
@section('js')
<script type="text/javascript">
    var selectUserID=[''];
    var selectUserName=[''];
    var selectUserBranch=[''];
    var selectUserImg=[''];
    var countUser=0;
    function changPerson(userID,userName, userBranch, userImg){
        
        var cardValue=document.getElementById('inp_'+userID).value;
        if(cardValue==1){ 
            document.getElementById('inp_'+userID).value=0; 
            //alert(userName+' '+cardValue);
            document.getElementById('usr_'+userID).style.background = "";
            for (i=0; i<countUser; i++) {
                if(userID==selectUserID[i]){   
                    selectUserID[i]='';
                    selectUserName[i]='';
                    selectUserBranch[i]='';
                    selectUserImg[i]='';
                }
            } 
            //document.getElementById("showName").innerHTML = selectUserName;
        }else{ 
            document.getElementById('inp_'+userID).value=1; 
            //alert(userName+' '+cardValue);
            document.getElementById('usr_'+userID).style.background = "#cccccc";
            selectUserID[countUser]=userID;
            selectUserName[countUser]=userName;
            selectUserBranch[countUser]=userBranch;
            selectUserImg[countUser]=userImg;
            //document.getElementById("showName").innerHTML = selectUserName; 
            countUser++; 
        }
        var showbtn=0;
        var showUser='';
        var showID="";
        var showBranch="";
        var showImg="";
        for (i=0; i<countUser; i++) {
            if(selectUserName[i]!=""){
                showbtn=1;
                if(showUser==''){
                    showUser=selectUserName[i];
                    showID=selectUserID[i];
                    showBranch=selectUserBranch[i];
                    showImg=selectUserImg[i];
                }else{
                    showUser=showUser+', '+selectUserName[i];
                    showID=showID+', '+selectUserID[i];
                    showBranch=showBranch+', '+selectUserBranch[i];
                    showImg=showImg+', '+selectUserImg[i];
                }
            }
        } 
        document.getElementById("showName").innerHTML = showUser;
        document.getElementById("userIDselect").value = showID;
        document.getElementById("userNameselect").value = showUser;
        document.getElementById("userBranchselect").value = showBranch;
        document.getElementById("userImgselect").value = showImg;
        
        if(showbtn==1){  
            document.getElementById('submitBTN').style.display=""; 
        }else{ 
            document.getElementById('submitBTN').style.display="none"; 
            document.getElementById("showName").innerHTML = '-';
        }
    }
</script>
@endsection
@section('content') 
@include('template.error')
<div class="row">
    <div class="col-lg container-fluid page__container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/')}}">หน้าหลัก</a></li>
            <li class="breadcrumb-item active">ข้อมูลผู้เข้าอบรม</li>
        </ol>
        <div style="text-align:center;">
        <h1 class="h2">ข้อมูลผู้เข้าอบรม</h1>
        <p class="card-subtitle">โปรเลือกผู้เข้าอบรมที่ต้องการ</p>
        </div>
        <div class="" style="margin-top:20px;">
            
            <div class="row"> 
                @foreach($personCanEnroll as $person)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="width:100%;" id="usr_{{$person['userCode']}}">
                            <div class="d-flex align-items-center">
                                <a href="#" class="mr-3" onClick="changPerson('{{$person['userCode']}}','{{$person['fullName']}}', '{{$person['branchName']}}', '{{$person['userImg']}}')">
                                    @if($person['userImg'])
                                    <img src="{{$person['userImg']}}" alt="" class="rounded-circle" width="50">
                                    @else
                                    <img src="{{asset('imgs/user.jpg')}}" alt="" class="rounded-circle" width="50">
                                    @endif
                                </a>
                                <input type="hidden" id="inp_{{$person['userCode']}}" value="0"/>
                                <div class="flex">
                                    <h5 class="mb-0">{{$person['fullName']}}</h5>
                                    <span class="badge badge-info">{{$person['branchName']}}</span>  
                                </div>
                            </div>
                            <small>ทักษะที่ผานการอบรมแล้ว</small>
                        </div> 
                        <!--
                        <ul class="list-group list-group-fit" > 
                            <li class="list-group-item" style="background-color:#EEEEEE;">
                                <a href="#" class="text-body text-decoration-0 d-flex align-items-center">
                                    <strong>Learn this course.</strong>
                                    <i class="material-icons text-muted ml-auto" style="font-size: inherit;">check</i>
                                </a>
                            </li>
                        </ul>
                        -->
                    </div> 
                </div>
                @endforeach  
            </div>
        </div> 
    </div>
    <div id="page-nav" class="col-lg-auto page-nav">
        <div data-perfect-scrollbar>
            <div class="page-section pt-lg-32pt">
                <h4 class="ml-3 mb-1 pl-2" style="background-color:black; color:white; padding:5px;">ลงทะเบียนร่วมอบรม</h4>
                <ul class="nav page-nav__menu">
                    <li class="nav-item" style="border-bottom:1px solid #ccc;">
                        <a href="{{url('/courses')}}" class="nav-link active" id="lnk_course">หลักสูตร <br/>
                        <small>{{$courseName}}</small></a>
                    </li>
                    <li class="nav-item" style="border-bottom:1px solid #ccc;">
                        <a href="{{url('/subjects?courseCode='.$courseCode.'&courseName='.$courseName)}}" class="nav-link active" id="lnk_subject">วิชา <br/>
                        <small>{{$subName}}</small>
                        </a>
                    </li>
                    <li class="nav-item" style="border-bottom:1px solid #ccc;">
                        <a href="{{url('/calendars?courseCode='.$courseCode.'&courseName='.$courseName.'&subCode='.$subCode.'&subName='.$subName)}}" class="nav-link active" id="lnk_calendar">รอบอบรม<br/>
                        <small>วันที่ {{$startDate}} ถึง {{$endDate}}</small><br/>
                        <small>เวลา {{$startTime}}-{{$endTime}}</small>
                        </a>
                    </li>
                    <li class="nav-item" style="border-left:2px solid red; border-bottom:1px solid #ccc;">
                        <a href="#" class="nav-link">ผู้เข้าอรมรม <br/>
                        <small id="showName">-</small></a>
                    </li>
                </ul> 
                <div style="padding-left:35px;">
                    <form method="post" action="{{url('/services')}}">
                        @csrf
                        <input type="hidden" name="courseCode" id="courseCode" value="{{$courseCode}}"/>
                        <input type="hidden" name="courseName" id="courseName" value="{{$courseName}}"/>
                        <input type="hidden" name="subCode" id="subCode" value="{{$subCode}}"/>
                        <input type="hidden" name="subName" id="subName" value="{{$subName}}"/>
                        <input type="hidden" name="trainCode" id="trainCode" value="{{$trainCode}}"/>
                        <input type="hidden" name="startDate" id="startDate" value="{{$startDate}}"/>
                        <input type="hidden" name="endDate" id="endDate" value="{{$endDate}}"/>
                        <input type="hidden" name="startTime" id="startTime" value="{{$startTime}}"/>
                        <input type="hidden" name="endTime" id="endTime" value="{{$endTime}}"/>
                        <input type="hidden" name="userIDselect" id="userIDselect" value=""/>
                        <input type="hidden" name="userNameselect" id="userNameselect" value=""/>
                        <input type="hidden" name="userBranchselect" id="userBranchselect" value=""/>
                        <input type="hidden" name="userImgselect" id="userImgselect" value=""/>
                        <button type="submit" style="display:none;" class="btn btn-danger btn-block" id="submitBTN">ลงทะเบียน</button> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection