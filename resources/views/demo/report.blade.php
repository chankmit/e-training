@extends('template.main')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">หน้าหลัก</a></li>
    <li class="breadcrumb-item active">Check In/Check Out</li>
</ol>

@endsection
@section('content') 
@include('template.error')
<div style="margin-top:20px;"> 
    <div class="row"> 
        <div class="col-md-2">
            
            <div style="padding:10px; background-color:black;">
                <a href="{{url('/checkin')}}" style="color:white;">Check In/Check Out</a>
            </div>
            <div style="padding:10px; background-color:black; border-left:5px solid red; margin-right:-5px;">
                <a href="{{url('/report')}}" style="color:white;">Report</a>
            </div>
            
        </div>
        <div class="col-md-3">
            Period Type:
            <div>
                <select name="" id="" class="form-control">
                    <option value="">Select Period Type</option>
                </select>
            </div>
            
            Period:
            <div>
                <select name="" id="" class="form-control">
                    <option value="">Select Period</option>
                </select> 
            </div>
            
            <br/>
            Report:
            <div>
                <div style="padding:10px; border:1px solid #cccccc; background-color:white; border-left:5px solid red;">
                    <a href="#">Session & Paricipants</a>
                </div>
                <div style="padding:10px; border:1px solid #cccccc; background-color:white;">
                    <a href="#">Enrollment Summary</a>
                </div>
                <div style="padding:10px; border:1px solid #cccccc; background-color:white;">
                    <a href="#">Enrollment Detail</a>
                </div>
                <div style="padding:10px; border:1px solid #cccccc; background-color:white;">
                    <a href="#">Attendance & No Show</a>
                </div> 
            </div>
        </div> 
        <div class="col-md-7"> 
            <div class="card">
                <div class="card-header">
                    <div class="media">
                        <div class="media-body">Session & Paricipants</div>
                        <div>
                            <i class="far fa-file-pdf"></i>
                            <i class="far fa-file-excel"></i>
                            <i class="fas fa-print"></i>
                        </div>
                    </div> 
                </div>
                <div class="card-body">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 