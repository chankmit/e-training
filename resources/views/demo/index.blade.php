@extends('template.home')
@section('css')
    <style type="text/css"> 
        .cover-image-full > img {
          width: 100%;
        } 
    </style>
@endsection
@section('home')

  <div style="text-align:center; position:fixed; margin-top: 50px; width:100%;">
    <p style="text-align:center; color:white; text-shadow:1px 1px 1px black;" style="">ยินดีต้อนรับสู่ระบบ</p>
    <h2 style="text-align:center; color:white; text-shadow:1px 1px 1px black; margin-top:-10px;">Training Management System</h2>
  </div>
  <div class="cover-image-full">
    <img src="{{asset('assets/images/bg.jpg')}}" alt="" id="img_cover" />
  </div>
@endsection