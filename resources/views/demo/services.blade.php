@extends('template.main')
@section('css')
<style type="text/css">
@supports (zoom:2) {
	input[type="radio"],  input[type=checkbox]{
	zoom: 2; 
	}
}
@supports not (zoom:2) {
	input[type="radio"],  input[type=checkbox]{
		transform: scale(2);
		margin: 15px; 
	}
}
label{ 
	display: inline-block;
	vertical-align: top;
	margin-top: 10px;
}
</style>
@endsection
@section('js')
<script type="text/javascript">
    $('.modal').parent().on('show.bs.modal', function (e) { $(e.relatedTarget.attributes['data-target'].value).appendTo('body'); })
    var showSelect="";
    function checkValue(chkId){  
        
        if(chkId=="op01"){
            //alert('01'); 
            var checkStatus= checkstatus=document.getElementById('op01').checked;
            if(checkStatus){
                showSelect='ไม่ต้องการบริการเสริม';
                document.getElementById('showSelect').innerHTML=showSelect;
                checkstatus=document.getElementById('op02').checked=false;
                //document.getElementById('op03').checked='false';
                $('.collapse').collapse('hide');
                document.getElementById('submitBTN').style.display=""; 
            }else{ 
                showSelect='ต้องการบริการเสริม';
                document.getElementById('showSelect').innerHTML=showSelect;
            }
        }else{   
            document.getElementById('showSelect').innerHTML='ต้องการบริการเสริม';
            checkstatus=document.getElementById('op01').checked=false;
        }
        
    }
    
    var checkUser=[''];
    var checkOption=[''];
    var countOption=0;
    function seclectDate(optionCode, userCode, selectDate, checkID){
        var checktype=document.getElementById(checkID).checked;

        if(checktype){ 
            countOption++;
            checkUser[countOption]=userCode;
            checkOption[countOption]=optionCode;
            checkDate[countOption]=selectDate;   
        }else{ 
            //alert(checktype); 
            for(i=0;i<=countOption;i++){ 
                if(checkUser[i]==userCode && checkOption[i]==optionCode && checkDate[i]==selectDate){
                    checkUser[i]='';
                    checkOption[i]='';
                    checkDate[i]='';
                }
            } 
        }
        
        var showCheckUser='';
        var showCheckOption='';
        var showCheckDate='';
        
        for(i=0;i<=countOption;i++){
            if(checkUser[i]!=''){
                if(showCheckUser==''){
                    showCheckUser=checkUser[i];
                }else{
                    showCheckUser=showCheckUser+','+checkUser[i];
                }
                
                if(showCheckOption==''){
                    showCheckOption=checkOption[i];
                }else{
                    showCheckOption=showCheckOption+','+checkOption[i];
                }
                
                if(showCheckDate==''){
                    showCheckDate=checkDate[i];
                }else{
                    showCheckDate=showCheckDate+','+checkDate[i];
                }
            }
        }
        
        document.getElementById('checkUser').value=showCheckUser;
        document.getElementById('checkOption').value=showCheckOption;
        document.getElementById('checkDate').value=showCheckDate;
    }
    
</script>
@endsection
@section('content') 
@include('template.error')
<form method="post" action="{{url('/regis')}}">
@csrf
<?php $countcheck=0; ?>
<div class="row">
    <div class="col-lg container-fluid page__container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/')}}">หน้าหลัก</a></li>
            <li class="breadcrumb-item active">บริการเสริม</li>
        </ol>
        <div style="text-align:center;">
            <h1 class="h2">ข้อมูลบริการเสริม</h1>
            <p class="card-subtitle">โปรดเลือกบริการเสริมที่ท่านต้องการ</p>
        </div>
        @foreach($optionList as $option)
        <div class="card card-body" style="margin-top:20px;"> 
            <div class="row"> 
                <div class="col-md-1">  
                    @if($option['optionCode']=='op01')
                         <input type="checkbox" checked name="opt[]" class="checkbox checkbox-primary" value="{{$option['optionCode']}}" onClick="checkValue('{{$option['optionCode']}}')" id="{{$option['optionCode']}}"  data-toggle="collapse" data-target="#collapse_{{$option['optionCode']}}" aria-expanded="false" aria-controls="collapse_{{$option['optionCode']}}"/>
                    @else
                        <input type="checkbox" name="opt[]" class="checkbox checkbox-primary" value="{{$option['optionCode']}}" onClick="checkValue('{{$option['optionCode']}}')" id="{{$option['optionCode']}}"  data-toggle="collapse" data-target="#collapse_{{$option['optionCode']}}" aria-expanded="false" aria-controls="collapse_{{$option['optionCode']}}"/>
                    @endif
                </div>
                <div class="col-md-11">
                    <div>{{$option['optionDetail']}}</div>
                </div>
            </div>   
        </div> 
        @if($option['requireSpecific']=="Y")
        <div class="collapse" id="collapse_{{$option['optionCode']}}" style="margin-top:-15px;">
          <div class="card">
            <div class="card-body">
                <div class="row">
                @foreach($userSelect as $person)
                    <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="width:100%;" id="usr_{{$person['userCode']}}">
                            <div class="d-flex align-items-center">
                                <a href="#" class="mr-3" >
                                    @if($person['userImg'])
                                    <img src="{{$person['userImg']}}" alt="" class="rounded-circle" width="50">
                                    @else
                                    <img src="{{asset('imgs/user.jpg')}}" alt="" class="rounded-circle" width="50">
                                    @endif
                                </a>
                                <input type="hidden" id="inp_{{$person['userCode']}}" value="0"/>
                                <div class="flex">
                                    <h5 class="mb-0">{{$person['fullName']}}</h5>
                                    <span class="badge badge-info">{{$person['branchName']}}</span>  
                                </div>
                            </div> 
                            <a class="btn btn-danger btn-block btn-sm mt-2" href="#" data-toggle="modal" data-target="#myModal_{{$option['optionCode']}}_{{str_replace(' ','',$person['userCode'])}}" data-backdrop="true" >เลือกวันที่ต้องการ</a>
                        </div> 
                        <!--
                        <small>หลักสูตรที่ผ่านการอบรมแล้ว</small>
                        <ul class="list-group list-group-fit" > 
                            <li class="list-group-item" style="background-color:#EEEEEE;">
                                <a href="#" class="text-body text-decoration-0 d-flex align-items-center">
                                    <strong>Learn this course.</strong>
                                    <i class="material-icons text-muted ml-auto" style="font-size: inherit;">check</i>
                                </a>
                            </li>
                        </ul>
                        -->
                    </div> 
                    </div>
                    
                    <!-- The Modal -->
                    <div class="modal fade" id="myModal_{{$option['optionCode']}}_{{str_replace(' ','',$person['userCode'])}}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                          <h4 class="modal-title">เลือกวันที่ต้องการบริการ</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body"> 
                        <?php 
                            $sl_sdate=explode('-',$startDate);
                            $sl_edate=explode('-',$endDate); 
                        ?>
                        @for ($i = $startDate; $i<=$endDate; $i++)
                            <div class="row">  
                                <div class="col-md-1">   
                                    <input type="checkbox" onClick="seclectDate('{{$option['optionCode']}}', '{{str_replace(' ','',$person['userCode'])}}', '{{$i}}', 'opt_user_{{$option['optionCode']}}_{{str_replace(' ','',$person['userCode'])}}_{{$i}}')" id="opt_user_{{$option['optionCode']}}_{{str_replace(' ','',$person['userCode'])}}_{{$i}}" class="checkbox checkbox-primary"/>
                                </div>   
                                <div class="col-md-11">
                                    <div>วันที่ {{$i}} </div>
                                </div>
                            </div>  
                        @endfor
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">บันทึกข้อมูล</button> 
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button> 
                        </div>
                      </div>
                    </div>
                    </div>
                @endforeach
                </div>
            </div> 
          </div> 
        </div>
        @endif
        @endforeach
         
    </div>
    <div id="page-nav" class="col-lg-auto page-nav">
        <div data-perfect-scrollbar>
            <div class="page-section pt-lg-32pt">
                <h4 class="ml-3 mb-1 pl-2" style="background-color:black; color:white; padding:5px;">ลงทะเบียนร่วมอบรม</h4>
                <ul class="nav page-nav__menu">
                    <li class="nav-item" style="border-bottom:1px solid #ccc;">
                        <a href="{{url('/courses')}}" class="nav-link active" id="lnk_course">หลักสูตร <br/>
                        <small>{{$courseName}}</small></a>
                    </li>
                    <li class="nav-item" style="border-bottom:1px solid #ccc;">
                        <a href="{{url('/subjects?courseCode='.$courseCode.'&courseName='.$courseName)}}" class="nav-link active" id="lnk_subject">วิชา <br/>
                        <small>{{$subName}}</small>
                        </a>
                    </li>
                    <li class="nav-item" style="border-bottom:1px solid #ccc;">
                        <a href="{{url('/calendars?courseCode='.$courseCode.'&courseName='.$courseName.'&subCode='.$subCode.'&subName='.$subName)}}" class="nav-link active" id="lnk_calendar">รอบอบรม<br/>
                        <small>วันที่ {{$startDate}} ถึง {{$endDate}}</small><br/>
                        <small>เวลา {{$startTime}}-{{$endTime}}</small>
                        </a>
                    </li>
                    <li class="nav-item" style="border-bottom:1px solid #ccc;">
                        <a href="{{url('/usersselect?courseCode='.$courseCode.'&courseName='.$courseName.'&subCode='.$subCode.'&subName='.$subName.'&trainCode='.$trainCode.'&startDate='.$startDate.'&endDate='.$endDate.'&startTime='.$startTime.'&endTime='.$endTime)}}" class="nav-link">ผู้เข้าอรมรม <br/>
                        <small>{{$userNameselect}}</small></a>
                    </li>
                    <li class="nav-item" style="border-left:2px solid red; border-bottom:1px solid #ccc;">
                        <a href="#" class="nav-link">บริการเสริม <br/>
                        <small id="showSelect">ไม่ต้องการบริการเสริม</small></a>
                    </li>
                </ul> 
                <div style="padding-left:35px;">
                        <input type="hidden" name="courseCode" id="courseCode" value="{{$courseCode}}"/>
                        <input type="hidden" name="courseName" id="courseName" value="{{$courseName}}"/>
                        <input type="hidden" name="subCode" id="subCode" value="{{$subCode}}"/>
                        <input type="hidden" name="subName" id="subName" value="{{$subName}}"/>
                        <input type="hidden" name="trainCode" id="trainCode" value="{{$trainCode}}"/>
                        <input type="hidden" name="startDate" id="startDate" value="{{$startDate}}"/>
                        <input type="hidden" name="endDate" id="endDate" value="{{$endDate}}"/>
                        <input type="hidden" name="startTime" id="startTime" value="{{$startTime}}"/>
                        <input type="hidden" name="endTime" id="endTime" value="{{$endTime}}"/>
                        <input type="hidden" name="userIDselect" id="userIDselect" value="{{$userIDselect}}"/>
                        <input type="hidden" name="userNameselect" id="userNameselect" value="{{$userNameselect}}"/>
                        <input type="hidden" name="userBranchselect" id="userBranchselect" value="{{$userBranchselect}}"/>
                        <input type="hidden" name="checkUser" id="checkUser" value=""/>
                        <input type="hidden" name="checkOption" id="checkOption" value=""/>
                        <input type="hidden" name="checkDate" id="checkDate" value=""/>
                        <button type="submit" style="display:;" class="btn btn-danger btn-block" id="submitBTN">ลงทะเบียน</button> 
                    
                </div>
            </div>
        </div>
    </div>
</div>
</form>

@endsection