@extends('template.main')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">หน้าหลัก</a></li>
    <li class="breadcrumb-item active">Check In/Check Out</li>
</ol>

@endsection
@section('content') 
@include('template.error')
<div style="margin-top:20px;"> 
    <div class="row"> 
        <div class="col-md-2"> 
            <div style="padding:10px; background-color:black;">
                <a href="{{url('/checkin')}}" style="color:white;">Check In/Check Out</a>
            </div>
            <div style="padding:10px; background-color:black;">
                <a href="{{url('/report')}}" style="color:white;">Report</a>
            </div> 
        </div> 
    </div>
</div>
@endsection
 