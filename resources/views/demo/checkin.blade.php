@extends('template.main')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">หน้าหลัก</a></li>
    <li class="breadcrumb-item active">Check In/Check Out</li>
</ol>

@endsection
@section('content') 
@include('template.error')
<div style="margin-top:20px;"> 
    <div class="row"> 
        <div class="col-md-2">
            
            <div style="padding:10px; background-color:black; border-left:5px solid red; margin-right:-5px;">
                <a href="{{url('/checkin')}}" style="color:white;">Check In/Check Out</a>
            </div>
            <div style="padding:10px; background-color:black;">
                <a href="{{url('/report')}}" style="color:white;">Report</a>
            </div>
            
        </div>
        <div class="col-md-2">
            <div style="text-align:center; border-bottom:5px solid black;">วิชา</div>
            @foreach($trainingSession as $train) 
            <div style="border-bottom:1px solid #cccccc; @if($train['trainingCode']==$trainingCode)  background-color:#eeeeee; @endif">
            <div class="media">
                <div class="media-body">
                    <div><a href="{{url('/checkin?trainingCode='.$train['trainingCode'].'&startDate='.$train['startDate'].'&endDate='.$train['endDate'].'&startTime='.$train['startTime'].'&endTime='.$train['endTime'])}}">{{$train['trainingName']}}</a></div>
                </div> 
                <div>
                    <i class="far fa-user"></i> 
                    {{$train['totalEnroll']??'-'}}
                </div>
            </div>
            <small>{{$train['sessionDate']}}</small>
            </div>
            @endforeach
        </div>
        <div class="col-md-2"> 
            <div style="text-align:center; border-bottom:5px solid black;">รอบ</div>
            @if($startDate)
            @for ($i = $startDate; $i<=$endDate; $i++) 
                <div class="media" style="border-bottom:1px solid #cccccc; @if($i==$selectDate) background-color:#eeeeee; @endif">
                    <div class="media-body">
                        <div><a href="{{url('/checkin?trainingCode='.$trainingCode.'&startDate='.$train['startDate'].'&endDate='.$train['endDate'].'&startTime='.$train['startTime'].'&endTime='.$train['endTime']).'&selectDate='.$i}}">
                            {{$i}}
                        </a></div>
                        <small>({{$startTime}}-{{$endTime}})</small>
                    </div> 
                    <div>
                        <i class="fas fa-qrcode mt-1" style="font-size:2em; color:#cccccc;"></i>
                   </div>
                </div>
            @endfor
            @endif
        </div>
        <div class="col-md-6"> 
            <div style="text-align:center; border-bottom:5px solid black;">ผู้เข้าอบรม</div>
            @if($selectDate)
            <div class="row" style="border-bottom:1px solid #cccccc;">
                <div class="col-md-4">
                    <div>สามารถ เรียนรู้ไว</div>
                </div>
                <div class="col-md-4">
                    <div>บริษัท มิซูมิชิ ธิงส์ จำกัด</div>
                </div>
                <div class="col-md-4">
                    <div>ยังไม่ได้ลงทะเบียน</div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
 