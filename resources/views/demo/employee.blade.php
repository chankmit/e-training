@extends('template.main')
@section('js')
<script type="text/javascript">
    $('.modal').parent().on('show.bs.modal', function (e) { $(e.relatedTarget.attributes['data-target'].value).appendTo('body'); })
</script>
@endsection 
@section('content') 
@include('template.error')
<div style="margin-top:20px;">
    <div> 
        <div class="card card-body border-left-3 border-left-primary navbar-shadow mb-4">
            <form action="#" id="frm_2">
                <div class="d-flex flex-wrap2 align-items-center mb-headings">
                    <div class="flex search-form mr-3 search-form--light">
                        <input type="text" class="form-control" placeholder="ค้นหาพนักงาน" id="txt">
                        <button class="btn" type="button" role="button"><i class="material-icons">search</i></button>
                    </div> 
                    <button class="btn btn-dark ml-3"><i class="material-icons">location_on</i> ทุกสาขา</button>
                </div>
 
            </form>
        </div>  
    </div>
    <div class="row">
        <div class="col-md-4"> 
            <div class="card">
                <div class="card-header">
                    <div class="media align-items-center">
                        <div class="media-body">
                            <p class="card-subtitle">ตำแหน่ง</p>
                            <h4 class="card-title">Sales</h4>
                        </div>
                        <div class="media-right">
                            <p class="card-subtitle">จำนวน</p>
                            <h4 class="card-title">20</h4> 
                        </div>
                    </div>
                </div>
                <p class="ml-3 mb-0">จำนวนผู้ผ่านการอบรมแต่ละวิชา</p>
                <img src="{{asset('assets/images/coin.png')}}" style="width:100%;">
            </div>
        </div> 
        <div class="col-md-8">
            <div class="row"> 
                <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <a href="#" class="mr-3" data-toggle="modal" data-target="#myModal" data-backdrop="true">
                                <img src="{{asset('assets/images/admin.png')}}" alt="" class="rounded-circle" width="60">
                            </a>
                            <div class="flex">
                                <p class="card-title mb-0">สามารถ เรียนรู้ไว</p>
                                <span class="badge badge-info">ผู้จัดการฝ่ายขาย</span> 
                                <br> 
                                <img src="{{asset('assets/images/usr_can.png')}}" height="35">
                            </div>
                        </div>
                    </div> 
                </div> 
                </div> 
                <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <a href="#" class="mr-3" data-toggle="modal" data-target="#myModal" data-backdrop="true">
                                <img src="{{asset('assets/images/admin.png')}}" alt="" class="rounded-circle" width="60">
                            </a>
                            <div class="flex">
                                <p class="card-title mb-0">สามารถ เรียนรู้ไว</p>
                                <span class="badge badge-info">ผู้จัดการฝ่ายขาย</span> 
                                <br> 
                                <img src="{{asset('assets/images/usr_can.png')}}" height="35">
                            </div>
                        </div>
                    </div> 
                </div> 
                </div> 
                
                <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <a href="#" class="mr-3" data-toggle="modal" data-target="#myModal" data-backdrop="true">
                                <img src="{{asset('assets/images/admin.png')}}" alt="" class="rounded-circle" width="60">
                            </a>
                            <div class="flex">
                                <p class="card-title mb-0">สามารถ เรียนรู้ไว</p>
                                <span class="badge badge-info">ผู้จัดการฝ่ายขาย</span> 
                                <br> 
                                <img src="{{asset('assets/images/usr_can.png')}}" height="35">
                            </div>
                        </div>
                    </div> 
                </div> 
                </div> 
                
                <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <a href="#" class="mr-3" data-toggle="modal" data-target="#myModal" data-backdrop="true">
                                <img src="{{asset('assets/images/admin.png')}}" alt="" class="rounded-circle" width="60">
                            </a>
                            <div class="flex">
                                <p class="card-title mb-0">สามารถ เรียนรู้ไว</p>
                                <span class="badge badge-info">ผู้จัดการฝ่ายขาย</span> 
                                <br> 
                                <img src="{{asset('assets/images/usr_can.png')}}" height="35">
                            </div>
                        </div>
                    </div> 
                </div> 
                </div> 
                
                <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <a href="#" class="mr-3" data-toggle="modal" data-target="#myModal" data-backdrop="true">
                                <img src="{{asset('assets/images/admin.png')}}" alt="" class="rounded-circle" width="60">
                            </a>
                            <div class="flex">
                                <p class="card-title mb-0">สามารถ เรียนรู้ไว</p>
                                <span class="badge badge-info">ผู้จัดการฝ่ายขาย</span> 
                                <br> 
                                <img src="{{asset('assets/images/usr_can.png')}}" height="35">
                            </div>
                        </div>
                    </div> 
                </div> 
                </div> 
                
                <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <a href="#" class="mr-3" data-toggle="modal" data-target="#myModal" data-backdrop="true">
                                <img src="{{asset('assets/images/admin.png')}}" alt="" class="rounded-circle" width="60">
                            </a>
                            <div class="flex">
                                <p class="card-title mb-0">สามารถ เรียนรู้ไว</p>
                                <span class="badge badge-info">ผู้จัดการฝ่ายขาย</span> 
                                <br> 
                                <img src="{{asset('assets/images/usr_can.png')}}" height="35">
                            </div>
                        </div>
                    </div> 
                </div> 
                </div> 
                
                <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <a href="#" class="mr-3" data-toggle="modal" data-target="#myModal" data-backdrop="true">
                                <img src="{{asset('assets/images/admin.png')}}" alt="" class="rounded-circle" width="60">
                            </a>
                            <div class="flex">
                                <p class="card-title mb-0">สามารถ เรียนรู้ไว</p>
                                <span class="badge badge-info">ผู้จัดการฝ่ายขาย</span> 
                                <br> 
                                <img src="{{asset('assets/images/usr_can.png')}}" height="35">
                            </div>
                        </div>
                    </div> 
                </div> 
                </div> 
                
                
            </div>
        </div> 
    </div>
</div>


<!-- The Modal -->
<div class="modal fade" id="myModal" style="margin-top:50px; margin-button:-50px;">
<div class="modal-dialog ">
  <div class="modal-content">
    <!-- Modal Header -->
    <div class="modal-header"> 
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <!-- Modal body -->
    <div class="modal-body">  

        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <a href="#" class="mr-3">
                        <img src="{{asset('assets/images/admin.png')}}" alt="" class="rounded-circle" width="100">
                    </a>
                    <div class="flex">
                        <h4 class="card-title mb-0">สามารถ เรียนรู้ไว</h4
                        <span class="badge badge-info">ผู้จัดการฝ่ายขาย</span> 
                        <br>  
                    </div>
                </div>
            </div>
            <img src="{{asset('assets/images/coin.png')}}" width="100%">
        </div> 
    </div>
    <!-- Modal footer -->
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
    </div>
  </div>
</div>
</div>
@endsection
 