<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 
Route::group(['middleware' => ['checkauth']], function () {
    Route::get('/', 'MainController@index');
    Route::get('/news', 'MainController@news');
    Route::get('/courses', 'MainController@courses');
    Route::get('/subjects', 'MainController@subjects');
    Route::get('/calendars', 'MainController@calendars');
    Route::get('/usersselect', 'MainController@usersselect');
    Route::get('/orders', 'MainController@orders');
    Route::get('/checkin', 'MainController@checkin');
    Route::get('/report', 'MainController@report');
    Route::get('/manage', 'MainController@manage');
    Route::post('/services', 'MainController@services');
    Route::post('/regis', 'MainController@regis');
    Route::get('/logout', 'MainController@logout');
});
Route::get('/login', 'MainController@login');
Route::post('/dologin', 'MainController@dologin');

Route::get('/positions', function () {
    $showpage='positions';
    return view('demo.position',['showpage'=>$showpage]);
}); 
Route::get('/employee', function () {
    $showpage='positions';
    return view('demo.employee',['showpage'=>$showpage]);
});  
